import scrapy


class QuotesSpider(scrapy.Spider):
    name = "quotes"

    def start_requests(self):
        urls = [
            'https://www.google.com/search?biw=1366&bih=637&tbs=cdr%3A1%2Ccd_min%3A1%2F1%2F2020%2Ccd_max%3A6%2F3%2F2020&tbm=nws&ei=jMXXXu-PPIbSsAXR-5_4BA&q=senado+de+la+republica+la+w&oq=senado+de+la+republica+la+w&gs_l=psy-ab.3...365584.368090.0.368374.16.10.0.0.0.0.538.1610.2-1j1j1j1.4.0....0...1c.1.64.psy-ab..15.0.0....0.Ex7Cf68Ci4k',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        stra = ''
        aux = 1
        for entry in response.xpath("//a/@href"):
            if ('https://www.wradio.com.co/' in entry.extract()):
                if not(entry.extract().replace('/url?q=', '').split('&')[0]+'\n' in stra):
                    stra += entry.extract().replace('/url?q=','').split('&')[0]+'\n'
        filename = 'links.txt'
        archivo = open(filename, 'a')
        archivo.write(stra)
        archivo.close()
        self.log('Saved file %s' % filename)
        next_page = response.xpath(
            "//a[@aria-label='Página siguiente']/@href").get()
        if next_page is not None:
            yield response.follow(next_page, callback=self.parse)
