import scrapy
import json

meses = {'enero': '01', 'febrero': '02', 'marzo': '03', 'abril': '04', 'mayo': '05', 'junio': '06',
         'julio': '07', 'agosto': '08', 'septiembre': '09', 'octubre': '10', 'noviembre': '11', 'diciembre': '12'}

news_list = []
urls_global = []
var = 0
var_true = 0


class QuotesSpider(scrapy.Spider):
    name = "girh"

    def start_requests(self):
        file = open("links.txt")
        line = file.readline()
        global var_true
        while line:
            line = file.readline()
            urls_global.append(line)
            var_true += 1
        file.close()

        for url in urls_global:
            yield scrapy.Request(url=url, callback=self.parse)
        

    def parse(self, response):

        personaje = 'senado_de_la_republica'

        final_new = {}

        final_new['titulo'] = response.xpath(
            '//h1[@itemprop="name headline"]/text()').get()

        final_new['complemento'] = response.xpath(
            '//div[@class="encabezado opinion"]/div/h2/text()').get()
        
        final_new['fecha'] = response.xpath('//span[@class="meta-time"]/text()').get().split(' ')[3]+'-'+meses[response.xpath('//span[@class="meta-time"]/text()').get().split(' ')[1]]+'-'+response.xpath('//span[@class="meta-time"]/text()').get().split(' ')[2]

        try:
            final_new['img'] = response.xpath(
                '//img[@itemprop="image"]/@src').get()
        except:
            final_new['img'] = None

        final_new['tags'] = None

        initial_text = ''

        for p in response.xpath('//div[@itemprop="articleBody"]/div/p/text()').getall():
            p = p.replace('Si encuentras un error de sintaxis, redacción u ortográfico en el artículo, selecciónalo y presiona', '')
            p = p.replace('para informarnos. ¡Muchas gracias!', '')
            initial_text = initial_text + p

        final_new['cuerpo'] = initial_text

        final_new['personaje'] = 'Senado De La Republica'

        final_new['medio'] = 'Las Dos Orillas'

        final_new['url'] = response.url

        final_new['tema'] = None

        final_new['sentimiento'] = None

        final_new['pertenece_a'] = 'Prensa'

        final_new['revisado_por'] = 0

        final_new['revisado'] = 0

        final_new['pertenencia'] = 0


        if ('senado ' in final_new['cuerpo'].lower() and '2020' in final_new['fecha']):
            news_list.append(final_new)
            print('holi')
        global var
        var += 1

        if (var == var_true - 1):
            print(personaje+'.json')
            file = open(personaje+'.json', 'w', encoding='utf8')

            file.write('[')
            file.write('\n')
            for new in news_list:
                json.dump(new, file, ensure_ascii=False)
                if new != news_list[-1]:
                    file.write(",")
                file.write("\n")
            file.write("]")

            print(personaje+'.json')

        print(var)
        print(var_true - 1)
        
        

       

        '''filename = 'petro-links.txt'
        archivo = open(filename, 'a')
        archivo.write(str)
        archivo.close()
        self.log('Saved file %s' % filename)
        next_page = response.xpath(
            "//a[@aria-label='Página siguiente']/@href").get()
        if next_page is not None:
            yield response.follow(next_page, callback=self.parse)'''
