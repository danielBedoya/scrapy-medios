import scrapy
import json
from os import remove


news_list = []
urls_global = []

meses = {'enero': '01', 'febrero': '02', 'marzo': '03', 'abril': '04', 'mayo': '05', 'junio': '06',
         'julio': '07', 'agosto': '08', 'septiembre': '09', 'octubre': '10', 'noviembre': '11', 'diciembre': '12'}


class QuotesSpider(scrapy.Spider):
    name = "ldos"

    def start_requests(self):
        file = open(self.personaje.lower().replace('á', 'a').replace('é', 'e').replace(
            'í', 'i').replace('ó', 'o').replace('ú', 'u').replace(' ', '_')+'_'+self.medio+'.txt')
        line = file.readline()
        while line:
            line = file.readline()
            urls_global.append(line)
        file.close()

        for url in urls_global:
            yield scrapy.Request(url=url, callback=self.parse)
        
        print('estoy acá')
        x = input()
        

    def parse(self, response):

        personaje = self.personaje.lower().replace('á', 'a').replace('é', 'e').replace(
            'í', 'i').replace('ó', 'o').replace('ú', 'u').replace('\n', '')

        if ('https://www.google.com' in response.url):
            print(personaje+'.json')
            file = open(personaje.replace(' ', '_')+'_' +
                        self.medio+'.json', 'w', encoding='utf8')

            file.write('[')
            file.write('\n')
            for new in news_list:
                json.dump(new, file, ensure_ascii=False)
                if new != news_list[-1]:
                    file.write(",")
                file.write("\n")
            file.write("]")

            print(personaje.replace(' ', '_')+'_'+self.medio+'.json')
            remove(self.personaje.lower().replace('á', 'a').replace('é', 'e').replace(
                'í', 'i').replace('ó', 'o').replace('ú', 'u').replace(' ', '_')+'_'+self.medio+'.txt')


        final_new = {}

        final_new['titulo'] = response.xpath(
            '//h1[@itemprop="name headline"]/text()').get()

        final_new['complemento'] = response.xpath(
            '//div[@class="encabezado opinion"]/div/h2/text()').get()
        
        final_new['fecha'] = response.xpath(
            '//span[@class="meta-time"]/text()').get().split(' ')[3]+'-'+meses[response.xpath(
                '//span[@class="meta-time"]/text()').get().split(' ')[1]] + '-' + response.xpath(
            '//span[@class="meta-time"]/text()').get().split(' ')[2].replace(',','')

        try:
            final_new['img'] = response.xpath(
                '//img[@itemprop="image"]/@src').get()
        except:
            final_new['img'] = None

        final_new['tags'] = None

        initial_text = ''

        for p in response.xpath('//div[@itemprop="articleBody"]/div/p/text()').getall():
            p = p.replace('Si encuentras un error de sintaxis, redacción u ortográfico en el artículo, selecciónalo y presiona', '')
            p = p.replace('para informarnos. ¡Muchas gracias!', '')
            initial_text = initial_text + p

        final_new['cuerpo'] = initial_text

        final_new['personaje'] = ''

        for palabra in self.personaje.split(' '):
            final_new['personaje'] += palabra.capitalize()+' '

        final_new['medio'] = 'Las Dos Orillas'

        final_new['url'] = response.url

        final_new['tema'] = None

        final_new['sentimiento'] = None

        final_new['pertenece_a'] = 'Prensa'

        final_new['revisado_por'] = 0

        final_new['revisado'] = 0

        final_new['pertenencia'] = 0

        if (self.personaje.lower().replace('\n', '') in final_new['cuerpo'].lower()):
            news_list.append(final_new)
            print('added ', response.url)
        
        else:
            cadenaParseada = final_new['cuerpo'].lower().replace('á', 'a').replace('é', 'e').replace(
                'í', 'i').replace('é', 'e').replace('ó', 'o').replace('ú', 'u')

            count = 0
            anterior = ''

            for palabra in personaje.lower().replace('á', 'a').replace('é', 'e').replace('í', 'i').replace('é', 'e').replace('ó', 'o').replace('ú', 'u').split(' '):
                
                if palabra in cadenaParseada:
                    if count >= 1:
                        inicioSupuesto = cadenaParseada.find(
                            anterior, 0, len(cadenaParseada))+len(anterior)+1
                        inicioActual = cadenaParseada.find(palabra, 0, len(cadenaParseada))

                        if inicioSupuesto == inicioActual or inicioSupuesto+1 == inicioActual or inicioSupuesto-1 == inicioActual or inicioSupuesto+2 == inicioActual:
                            news_list.append(final_new)
                            print('added ', response.url)
                            break
                    count += 1
                    anterior = palabra
