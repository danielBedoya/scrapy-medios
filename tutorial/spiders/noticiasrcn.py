import scrapy
import json

news_list = []
urls_global = []
var = 0
var_true = 0


class QuotesSpider(scrapy.Spider):
    name = "nrcn"

    def start_requests(self):
        file = open("links.txt")
        line = file.readline()
        global var_true
        while line:
            line = file.readline()
            urls_global.append(line)
            var_true += 1
        file.close()

        for url in urls_global:
            yield scrapy.Request(url=url, callback=self.parse)
        

    def parse(self, response):

        personaje = 'senado_de_la_republica'

        final_new = {}

        final_new['titulo'] = response.xpath(
            "//div[@class='bloque-texto-nota']//h1/text()").get()

        final_new['fecha'] = response.xpath(
            "//article[@class='newArticle']/@data-article-publish-date").get()

        try:
            final_new['img'] = response.xpath(
                "//div[@class='bloque-nota-completa']//img/@src").extract()
        except:
            final_new['img'] = ''

        final_new['tags'] = []

        try:
            final_new['tags'] = response.xpath(
                "//meta[@name='keywords']/@content").extract()
        except:
            final_new['tags'] = ''

        initial_text = ''

        for p in response.xpath("//div[@class='bloque-nota-completa']").getall():
            var3 = 0
            str1 = ''
            for l in p:
                if ('<' in l):
                    var3 = 1
                if not(var3 == 1):
                    str1 += l

                if ('>' in l):
                    var3 = 0
            initial_text = initial_text + str1

        final_new['cuerpo'] = initial_text

        final_new['personaje'] = 'Senado De La Republica'

        final_new['medio'] = 'Noticias RCN'

        final_new['url'] = response.url

        final_new['tema'] = None

        final_new['sentimiento'] = None
        if ('senado ' in final_new['cuerpo'].lower()):
            news_list.append(final_new)
            print('holi')
        
        final_new['pertenece_a'] = 'Television'

        final_new['revisado_por'] = 0

        final_new['revisado'] = 0

        final_new['pertenencia'] = 0

        global var

        var += 1

        if (var == var_true - 1):
            print(personaje+'.json')
            file = open(personaje+'.json', 'w', encoding='utf8')

            file.write('[')
            file.write('\n')
            for new in news_list:
                json.dump(new, file, ensure_ascii=False)
                if new != news_list[-1]:
                    file.write(",")
                file.write("\n")
            file.write("]")

            print(personaje+'.json')

        print(var)
        print(var_true - 1)
        
        

       

        '''filename = 'petro-links.txt'
        archivo = open(filename, 'a')
        archivo.write(str)
        archivo.close()
        self.log('Saved file %s' % filename)
        next_page = response.xpath(
            "//a[@aria-label='Página siguiente']/@href").get()
        if next_page is not None:
            yield response.follow(next_page, callback=self.parse)'''
