import scrapy
import sys
from os import remove



class QuotesSpider(scrapy.Spider):
    name = "quotes"

    def start_requests(self):
        urls = [self.url]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        stra = ''
        aux = 1
        for entry in response.xpath("//a/@href"):
            if (self.direccion in entry.extract()):
                if not(entry.extract().replace('/url?q=', '').split('&')[0]+'\n' in stra):
                    stra += entry.extract().replace('/url?q=','').split('&')[0]+'\n'
        filename = self.personaje+' '+self.medio.replace('_',' ')+'1.txt'
        archivo = open(filename, 'a')
        archivo.write(stra)
        archivo.close()
        next_page = response.xpath(
            "//a[@aria-label='Página siguiente']/@href").get()
        if next_page is not None:
            yield response.follow(next_page, callback=self.parse)
        else:
            filename = self.personaje.replace(' ','_')+'_'+self.medio+'.txt'
            archivo = open(filename, 'a')
            filename1 = self.personaje+' '+self.medio.replace('_', ' ')+'1.txt'
            archivo1 = open(filename1)
            archivo.write('\n')
            for line in archivo1.readlines():
                archivo.write(line)
            archivo.write('https://www.google.com')
            archivo.close()
            archivo1.close()
            remove(filename1)
            
