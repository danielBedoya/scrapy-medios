import os

medios = ['las_dos_orillas']

direcciones = {'la_w': 'https://www.wradio.com.co/', 'las_dos_orillas':
               'https://www.las2orillas.co/', 'rcn_tv': 'https://noticias.canalrcn.com/'}

f = open('senadores.txt')
lines = f.readlines()
for line in lines:
    stringUrl = "https://www.google.com/search?q="
    for palabra in line.split(' '):
        stringUrl += palabra+'+'
    for medio in medios:
        personaje = line.lower().replace('á','a').replace('é','e').replace('í','i').replace('ó','o').replace('ú','u').replace('\n','')
        os.system("scrapy crawl quotes -a url='"+stringUrl+medio.replace('_','+')+"&source=lnms&tbm=nws' -a medio='"+medio+"' -a direccion='"+direcciones[medio]+"' -a personaje='"+personaje+"'")
    break

for line in lines:
    for medio in medios:
        personaje = line.lower().replace('\n', '')
        os.system("scrapy crawl ldos -a personaje='"+personaje+"' -a medio='"+medio+"'")
        os.system("python guardarenbd.py '"+ personaje +"' "+medio)
    break
