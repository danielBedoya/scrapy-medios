import os
import sys
import json
import datetime
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client['semana_ia']
collection_currency = db['prensa']
collection_currency.remove({})

schema_default = {
	'personaje': '',
	'medio': '',
	'fecha': '',
	'titulo': '',
	'cuerpo': '',
	'url': '',
	'complemento': '',
	'img': '',
	'tags': [],
	'sentimiento': '',
	'tema': '',
	'pertenencia': 0,
	'revisado': 0,
	'revisado_por': 0,
}


def get_article(data):
	if not data['titulo'] or data['titulo'].strip() == '':
		return None
	if not data['cuerpo'] or data['cuerpo'].strip() == '':
		return None
	if not data['fecha'] or data['fecha'].strip() == '':
		return None

	article = {}

	# Some special cases
	fecha = data['fecha'].strip()
	if fecha[6] == '-':  # cases -> 2020-2-02
		data['fecha'] = fecha[:5] + '0' + fecha[5:]

	data['personaje'] = data['personaje'].replace('de la', 'De La')

	fecha = datetime.datetime.fromisoformat(data['fecha'])
	if fecha.year < 2020:
		return None
	article['fecha'] = str(fecha)

	for key in schema_default:
		if key in data:
			article[key] = data[key]
		else:
			article[key] = schema_default[key]

	return article


cnt = 0
diff = {}

personaje = sys.argv[1].lower().replace(
	'á', 'a').replace('é', 'e').replace('í', 'i').replace('ó', 'o').replace('ú', 'u')
medio = sys.argv[2]

print (sys.argv)
filepath = personaje.replace(' ','_')+'_'+ medio + '.json'
with open(filepath, encoding="utf8") as f:
    file_data = json.load(f)
    for data in file_data:
        article = get_article(data)
        if article:
            collection_currency.insert(article)
